const Main = imports.ui.main;

function enable() {
    //Enable long swipes
    Main.wm._workspaceAnimation._swipeTracker.allowLongSwipes = true
}

function disable() {
    //Disable long swipes
    Main.wm._workspaceAnimation._swipeTracker.allowLongSwipes = false
}